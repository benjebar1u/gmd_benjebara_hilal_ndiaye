package services;

import java.sql.SQLException;

import objects.DiseaseRequest;
import objects.DiseaseResult;
import dao.CouchDao;
import dao.SaxReader;
import dao.SqlDao;
import dao.TxtReader;

public class DiseaseManagerService {

	class XmlAndCouchDbRunnable implements Runnable {
		private final DiseaseRequest request;

		public XmlAndCouchDbRunnable(final DiseaseRequest request) {
			this.request = request;
		}

		@Override
		public void run() {
			xmlDatasource.searchByDisease(request.getDisease(), result);
			couchDbDatasource.getDiseaseByName(request.getDisease(), result);
		}
	}

	SqlDao sqlDatasource;

	SaxReader xmlDatasource;

	TxtReader txtDatasource;

	CouchDao couchDbDatasource;

	DiseaseRequest request;

	DiseaseResult result;

	public DiseaseManagerService(final DiseaseRequest request) {
		result = new DiseaseResult(request.getDisease());
		this.request = request;
		sqlDatasource = new SqlDao();
		txtDatasource = new TxtReader();
		xmlDatasource = new SaxReader();
		couchDbDatasource = new CouchDao();
	}

	public DiseaseResult findInformationForGivenDisease(
			final DiseaseRequest request) {

		final Thread thread = new Thread(new XmlAndCouchDbRunnable(request));
		thread.run();

		sqlDatasource.findCausingDrugsByDisease(request.getDisease(), result);
		txtDatasource.searchByDisease(request.getDisease(), result);
		try {
			if(couchDbDatasource.getDiseasesBySynonym(request.getDisease(), result).getName()!= request.getDisease()){
				couchDbDatasource.getDiseasesBySynonym(request.getDisease(), result);
				String name=result.getDisease();
				xmlDatasource.searchByDisease(name, result);
				sqlDatasource.findCausingDrugsByDisease(name, result);
				
			}
		} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		try {
			sqlDatasource.getJdbcConnection().close();
		} catch (final SQLException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
			}
		
		return result;
	}

	public CouchDao getCouchDbDatasource() {
		return couchDbDatasource;
	}

	public DiseaseRequest getRequest() {
		return request;
	}

	public DiseaseResult getResult() {
		return result;
	}

	public SqlDao getSqlDatasource() {
		return sqlDatasource;
	}

	public TxtReader getTxtDatasource() {
		return txtDatasource;
	}

	public SaxReader getXmlDatasource() {
		return xmlDatasource;
	}

	public void setCouchDbDatasource(final CouchDao couchDbDatasource) {
		this.couchDbDatasource = couchDbDatasource;
	}

	public void setRequest(final DiseaseRequest request) {
		this.request = request;
	}

	public void setResult(final DiseaseResult result) {
		this.result = result;
	}

	public void setSqlDatasource(final SqlDao sqlDatasource) {
		this.sqlDatasource = sqlDatasource;
	}

	public void setTxtDatasource(final TxtReader txtDatasource) {
		this.txtDatasource = txtDatasource;
	}

	public void setXmlDatasource(final SaxReader xmlDatasource) {
		this.xmlDatasource = xmlDatasource;
	}

	public void source() {

	};

}
