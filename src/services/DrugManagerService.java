package services;

import dao.SaxReader;
import dao.SqlDao;
import objects.DrugResult;

public class DrugManagerService {
	SqlDao sqlDatasource;

	SaxReader xmlDatasource;

	DrugResult result;

	public DrugManagerService(String drug) {
		this.result = new DrugResult(drug);
		this.sqlDatasource = new SqlDao();
		this.xmlDatasource = new SaxReader();

	}

	public DrugResult findInformationForGivingDrug(String drug) {
		xmlDatasource.searchByDrug(drug, result);
		//sqlDatasource.findSideEffectsByDrug(drug, result);
		//sqlDatasource.findCuredDiseaseByDrug(drug, result);
		return result;
	}

	public SqlDao getSqlDatasource() {
		return sqlDatasource;
	}

	public void setSqlDatasource(SqlDao sqlDatasource) {
		this.sqlDatasource = sqlDatasource;
	}

	public SaxReader getXmlDatasource() {
		return xmlDatasource;
	}

	public void setXmlDatasource(SaxReader xmlDatasource) {
		this.xmlDatasource = xmlDatasource;
	}

	public DrugResult getResult() {
		return result;
	}

	public void setResult(DrugResult result) {
		this.result = result;
	}

}
