package dao;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;

import objects.DiseaseResult;
import objects.DrugResult;

import org.w3c.dom.DOMException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.SaxObject;

public class SaxReader extends DefaultHandler {
	private List<SaxObject> drugs = new LinkedList<SaxObject>();
	private SaxObject drug = new SaxObject();
	private StringBuffer buffer = new StringBuffer();
	boolean drugId = false;
	boolean dontAdd = false;
	
	public static boolean diseaseSax = false;
	public static boolean drugSax = false;

	public SaxReader() {
		super();
		try {
			final SAXParserFactory factory = SAXParserFactory.newInstance();
			final SAXParser parser = factory.newSAXParser();
			parser.parse("drugbank.xml", this);
		} catch (final DOMException e) {
			e.printStackTrace();
		} catch (final ParserConfigurationException e) {
			e.printStackTrace();
		} catch (final TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (final SAXException e) {
			e.printStackTrace();
		} catch (final IOException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void characters(final char[] ch, final int start, final int length)
			throws SAXException {

		final String lecture = new String(ch, start, length);
		if (buffer != null) {
			buffer.append(lecture);
		}

	}

	@Override
	public void endElement(final String uri, final String localName,
			final String qName) throws SAXException {

		if (qName.equals("drugs")) {
		} else if (qName.equals("drug")) {
			drugs.add(drug);
			drug = new SaxObject();

		} else if (qName.equals("drugbank-id")) {
			if (!dontAdd && drugId) {
				drug.setDrugbankId(buffer.toString());
			}
			buffer = null;

		} else if (qName.equals("name")) {

			buffer.toString();
			if (!dontAdd) {
				drug.setName(buffer.toString());
			}
			buffer = null;
		} else if (qName.equals("indication")) {
			drug.setIndication(buffer.toString());
			buffer = null;
		} else if (qName.equals("toxicity")) {
			drug.setToxicity(buffer.toString());
			buffer = null;
		} else if (qName.equals("synonym")) {
			drug.setSynonym(buffer.toString());
			buffer = null;
		} else if (qName.equals("products")) {
			dontAdd = false;
		} else if (qName.equals("mixtures")) {
			dontAdd = false;
		} else if (qName.equals("packagers")) {
			dontAdd = false;
		} else if (qName.equals("target")) {
			dontAdd = false;
		} else if (qName.equals("pfams")) {
			dontAdd = false;
		} else if (qName.equals("carriers")) {
			dontAdd = false;
		} else if (qName.equals("international-brands")) {
			dontAdd = false;
		} else if (qName.equals("transporter")) {
			dontAdd = false;
		} else if (qName.equals("drug-interaction")) {
			dontAdd = false;
		}
	}

	public void searchByDisease(final String disease, final DiseaseResult result) {
		for (final SaxObject d : drugs) {
			if (d.getIndication().indexOf(" " + disease) != -1) {
				result.getCuringDrugs().add(d.getName());
				//diseaseSax = true;
				result.addSource("DrugBank");
			}
			if (d.getToxicity().indexOf(" " + disease) != -1) {
				result.getCausingDrugs().add(d.getName());
				//diseaseSax = true;
				result.addSource("DrugBank");
			}
		}

	}

	public void searchByDrug(String drug, final DrugResult result) {
		if (drug.contains("*")) {
			final String values[] = drug.split("\\*");
			for (final SaxObject d : drugs) {
				if (d.getName() != null) {
					if (values.length > 1) {
						if (d.getName().startsWith(values[0])
								&& d.getName().endsWith(values[1])) {

							drug = d.getName();
							result.setDrug(drug);
							//drugSax = true;
							result.addSource("DrugBank");
						}
					} else {
						if (d.getName().startsWith(values[0])) {
							drug = d.getName();
							result.setDrug(drug);
							//drugSax = true;
							result.addSource("DrugBank");
						}
					}
				}
			}

		}

		for (final SaxObject d : drugs) {
			if (drug.equals(d.getName())) {
				result.getCuredDiseases().add(d.getIndication());
				result.getSideEffects().add(d.getToxicity());
			}
		}

	}

	@Override
	public void startElement(final String uri, final String localName,
			final String qName, final Attributes attributes)
			throws SAXException {
		if (qName.equals("drugs")) {
			drugs = new LinkedList<SaxObject>();
		} else if (qName.equals("drug")) {

			drug = new SaxObject();
			try {
				final String type = attributes.getValue("type");
				drug.setType(type);
			} catch (final Exception e) {
				throw new SAXException(e);
			}

		} else {
			buffer = new StringBuffer();
			if (qName.equals("drugbank-id")) {
				if ("true".equals(attributes.getValue("primary"))) {
					drugId = true;
				}
			} else if (qName.equals("name")) {
			} else if (qName.equals("indication")) {
			} else if (qName.equals("toxicity")) {
			} else if (qName.equals("synonym")) {
			} else if (qName.equals("products")) {
				dontAdd = true;
			} else if (qName.equals("mixtures")) {
				dontAdd = true;
			} else if (qName.equals("packagers")) {
				dontAdd = true;
			} else if (qName.equals("target")) {
				dontAdd = true;
			} else if (qName.equals("pfams")) {
				dontAdd = true;
			} else if (qName.equals("carriers")) {
				dontAdd = true;
			} else if (qName.equals("international-brands")) {
				dontAdd = true;
			} else if (qName.equals("transporter")) {
				dontAdd = true;
			} else if (qName.equals("drug-interaction")) {
				dontAdd = true;
			}
		}
	}

}