package dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;

import objects.DiseaseResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utils.CouchObject;

public class CouchDao {
	public static boolean diseaseCouch = false;

	class CouchDbRunnabe implements Runnable {

		String name;
		DiseaseResult result;

		public CouchDbRunnabe(final String name, final DiseaseResult result) {
			super();
			this.name = name;
			this.result = result;
		}

		@Override
		public void run() {
			try {
				// name = URLEncoder.encode(name, "UTF-8");
				name = name.replace("+", " ");
				String inputLine = null;
				final StringBuffer buff = new StringBuffer();
				final String values[] = name.split("\\*");
				final String url = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName";
				connectDB(url);
				final BufferedReader in = new BufferedReader(
						new InputStreamReader(conn.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					buff.append(inputLine + "\n");
					//diseaseCouch = true;
					result.addSource("OrphaData");
				}
				int orphaNumber = 0;
				in.close();
				final String resultX = buff.toString();
				final JSONObject obj = new JSONObject(resultX);
				final JSONArray disease = obj.getJSONArray("rows");
				for (int i = 0; i < disease.length(); i++) {
					final JSONObject diseaseObject = disease.getJSONObject(i);
					final JSONObject valueObject = diseaseObject
							.getJSONObject("value");
					if (values.length > 1) {

						if (diseaseObject.getString("key")
								.startsWith(values[0])
								&& diseaseObject.getString("key").endsWith(
										values[1])) {
							name = diseaseObject.getString("key");
							orphaNumber = valueObject.getInt("OrphaNumber");
							result.getSymptoms().add(
									getDiseaseClinicalSigns(orphaNumber));
							result.setDisease(diseaseObject.getString("key"));
						}
					} else {
						if (diseaseObject.getString("key")
								.startsWith(values[0])) {
							name = diseaseObject.getString("key");
							orphaNumber = valueObject.getInt("OrphaNumber");
							result.getSymptoms().add(
									getDiseaseClinicalSigns(orphaNumber));
							result.setDisease(diseaseObject.getString("key"));
						}
					}
				}
			} catch (final UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	static String HOST = "http://couchdb.telecomnancy.univ-lorraine.fr/";

	static String DB = "orphadatabase/";

	private HttpURLConnection conn;

	public void connectDB(final String url) throws MalformedURLException,
			IOException {
		conn = (HttpURLConnection) new URL(url).openConnection();
		conn.setReadTimeout(10000);
		conn.setConnectTimeout(15000);
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.setDoOutput(true);
	}

	public CouchObject getDiseaseByName(String name, final DiseaseResult result) {

		final CouchObject r = new CouchObject();
		try {
			name = URLEncoder.encode(name, "UTF-8");
			String inputLine = null;
			final StringBuffer buff = new StringBuffer();
			if (name.contains("*")) {
				final Thread thread = new Thread(new CouchDbRunnabe(name,
						result));
				thread.run();
				result.setDisease(name);

			} else {

				final String url = HOST + DB
						+ "_design/diseases/_view/GetDiseasesByName?key=\""
						+ name + "\"";
				connectDB(url);
				final BufferedReader in = new BufferedReader(
						new InputStreamReader(conn.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					buff.append(inputLine + "\n");
					diseaseCouch = true;
					result.addSource("OrphaData");
				}

				in.close();
				final String resultX = buff.toString();
				final JSONObject obj = new JSONObject(resultX);
				final JSONArray rows = obj.getJSONArray("rows");
				for (int i = 0; i < rows.length(); i++) {

					final JSONObject rowObject = rows.getJSONObject(i);
					r.setName(rowObject.getString("key"));
					result.setDisease(rowObject.getString("key"));
					final JSONObject valueObject = rowObject
							.getJSONObject("value");
					r.setOrphaNumber(valueObject.getInt("OrphaNumber"));
					
					
					if((getDiseaseClinicalSigns(r.getOrphaNumber())!=null)){
						diseaseCouch = true;
						/*if(result.getSource() == null ){
							ArrayList<String> temp = new ArrayList<String>(Arrays.asList("Orpha"));
							result.setSource(temp);
						}else {
							if(!result.getSource().contains("Orpha")){
									result.getSource().add("Orpha");}}*/
						
					final String values[] = getDiseaseClinicalSigns(
							r.getOrphaNumber()).split(",");
					
					for (final String s : values) {
						result.getSymptoms().add(s);
					}
					
					
					} 
			}
		
			} }catch (final UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return r;
	}

	public String getDiseaseClinicalSigns(final int orpha)
			throws MalformedURLException, IOException {
		String clinicalSigns = null;
		String inputLine = null;
		final StringBuffer buff = new StringBuffer();
		final String url1 = HOST
				+ DB
				+ "_design/clinicalsigns/_view/GetDiseaseClinicalSignsNoLang?key="
				+ orpha + "";
		connectDB(url1);
		final BufferedReader in = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			buff.append(inputLine + "\n");
		}
		in.close();
		final String s = buff.toString();
		JSONObject obj;
		try {
			obj = new JSONObject(s);
			final JSONArray rows = obj.getJSONArray("rows");
			for (int i = 0; i < rows.length(); i++) {
				final JSONObject rowObject = rows.getJSONObject(i);
				final JSONObject valueObject = rowObject.getJSONObject("value");
				final JSONObject clinicalObject = valueObject
						.getJSONObject("clinicalSign");
				final JSONObject nameObject = clinicalObject
						.getJSONObject("Name");
				final String text = nameObject.getString("text");
				// System.out.println(text);
				if (clinicalSigns == null) {

					clinicalSigns = text + ",";
				} else {

					clinicalSigns = clinicalSigns.concat(text + ",");
				}
			}
		} catch (final JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return clinicalSigns;
	}

	public CouchObject getDiseasesBySynonym(String Syn, final DiseaseResult result)
			throws MalformedURLException, IOException {
		final CouchObject r = new CouchObject();
		Syn = URLEncoder.encode(Syn, "UTF-8");
		String inputLine = null;
		final StringBuffer buff = new StringBuffer();
		final String url2 = HOST + DB
				+ "_design/diseases/_view/GetDiseasesBySynonym?key=\"" + Syn
				+ "\"";
		connectDB(url2);
		final BufferedReader in = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			buff.append(inputLine + "\n");
			//diseaseCouch = true;
			result.addSource("OrphaData");
		}
		in.close();
		final String s = buff.toString();
		JSONObject obj;
		try {
			obj = new JSONObject(s);
			final JSONArray rows = obj.getJSONArray("rows");
			for (int i = 0; i < rows.length(); i++) {
				final JSONObject rowObject = rows.getJSONObject(i);
				final JSONObject valueObject = rowObject.getJSONObject("value");
				final JSONObject NameObject = valueObject.getJSONObject("Name");
				result.setDisease(NameObject.getString("text"));
				getDiseaseByName(NameObject.getString("text"), result);
				//diseaseCouch = true;
				/*if(result.getSource() == null ){
					ArrayList<String> temp = new ArrayList<String>(Arrays.asList("Orpha as Synonym"));
					result.setSource(temp);
				}else {
					if(!result.getSource().contains("Orpha as Synonym")){
							result.getSource().add("Orpha as Synonym");}}*/
				

			}
		}catch (final JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return r;
	};

}

