package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;

import objects.DiseaseRequest;
import objects.DiseaseResult;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import utils.TxtObject;

public class TxtReader {
	TxtObject disease;

	public static boolean diseaseText = false;
	
	public TxtReader() {
		indexing();
	}

	public void getAllDiseases(final String prefix, final String suffix,
			final DiseaseRequest request) {
		final String index = "indexTXT";
		final String field = "disease";
		final int hitsPerPage = 1000;

		IndexReader reader;
		try {
			reader = DirectoryReader.open(FSDirectory.open(new File(index)));
			final IndexSearcher searcher = new IndexSearcher(reader);
			final Analyzer analyzer = new StandardAnalyzer();
			new BufferedReader(new InputStreamReader(System.in,
					StandardCharsets.UTF_8));
			// :Post-Release-Update-Version.LUCENE_XY:
			final QueryParser parser = new QueryParser(field, analyzer);
			if (parser.toString().startsWith(prefix)
					&& parser.toString().endsWith(suffix)) {
				final Query query = parser.parse(parser.toString());

				final TopScoreDocCollector collector = TopScoreDocCollector
						.create(hitsPerPage, true);
				searcher.search(query, collector);
				final ScoreDoc[] results = collector.topDocs().scoreDocs;
				for (final ScoreDoc result : results) {
					final int DocId = result.doc;
					final Document d = searcher.doc(DocId);
					request.getDiseases().add(d.get("disease"));
					diseaseText = true;
				}

			}

			reader.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// :Post-Release-Update-Version.LUCENE_XY:
		catch (final ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void indexDocs(final IndexWriter writer, final File file)
			throws IOException {
		if (file.canRead()) {
			FileInputStream fis = null;

			try {
				fis = new FileInputStream(file);
				final InputStreamReader isr = new InputStreamReader(fis);
				final BufferedReader br = new BufferedReader(isr);
				String line;
				String disease = "";
				String Symptom = "";
				String number = "";
				Document doc = new Document();
				while (!(line = br.readLine()).equals("*THEEND*")) {
					if (line.equals("*RECORD*")) {
						disease = "";
						Symptom = "";
						number = "";
						final Document doc1 = new Document();
						doc = doc1;
					}
					if (line.equals("*FIELD* NO")) {

						while (!(line = br.readLine()).startsWith("*FIELD*")) {
							number = number + line;
						}
						doc.add(new TextField("number", number, Field.Store.YES));
					}

					if (line.equals("*FIELD* TI")) {

						while (!(line = br.readLine()).startsWith("*FIELD*")) {
							disease = disease + line;
						}
						doc.add(new TextField("disease", disease,
								Field.Store.YES));
					}
					if (line.equals("*FIELD* CS")) {
						while (!(line = br.readLine()).startsWith("*FIELD*")) {
							Symptom = Symptom + " " + line;

						}
						doc.add(new StringField("Symptom", Symptom,
								Field.Store.YES));
						writer.addDocument(doc);
					}
					if (line.equals("*FIELD* ED")) {
					}
				}
				br.close();
			} catch (final FileNotFoundException fnfe) {
				fnfe.printStackTrace();
			} finally {
				fis.close();
			}
		}
	}

	@SuppressWarnings({ "deprecation", "unused" })
	public void indexing() {
		final String usage = "java org.apache.lucene.demo.IndexFiles"
				+ " [-index INDEX_PATH] [-docs DOCS_PATH] [-update]\n\n"
				+ "This indexes the documents in DOCS_PATH, creating a Lucene index"
				+ "in INDEX_PATH that can be searched with SearchFiles";
		final String indexPath = "indexTXT";
		final String docsPath = "omim.txt";
		final boolean create = true;

		if (docsPath == null) {
			System.err.println("Usage: " + usage);
			System.exit(1);
		}

		final File docDir = new File(docsPath);// file of indexation
		if (!docDir.exists() || !docDir.canRead()) {
			System.out
					.println("Document directory '"
							+ docDir.getAbsolutePath()
							+ "' does not exist or is not readable, please check the path");
			System.exit(1);
		}

		final Date start = new Date();
		try {
			final Directory dir = FSDirectory.open(new File(indexPath));// getting
																		// txt
																		// files
			final Analyzer analyzer = new StandardAnalyzer(
					Version.LUCENE_4_10_0);
			final IndexWriterConfig iwc = new IndexWriterConfig(
					Version.LUCENE_4_10_0, analyzer);

			if (create) {
				iwc.setOpenMode(OpenMode.CREATE);
			} else {
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}

			final IndexWriter writer = new IndexWriter(dir, iwc);
			indexDocs(writer, docDir);
			writer.close();
		} catch (final IOException e) {
			System.out.println(" caught a " + e.getClass()
					+ "\n with message: " + e.getMessage());
		}
	}

	public void searchByDisease(final String disease,
			final DiseaseResult resultD) {
		final String index = "indexTXT";
		final String field = "disease";
		final int hitsPerPage = 1000;

		IndexReader reader;
		try {
			reader = DirectoryReader.open(FSDirectory.open(new File(index)));
			final IndexSearcher searcher = new IndexSearcher(reader);
			final Analyzer analyzer = new StandardAnalyzer();
			new BufferedReader(new InputStreamReader(System.in,
					StandardCharsets.UTF_8));
			// :Post-Release-Update-Version.LUCENE_XY:
			final QueryParser parser = new QueryParser(field, analyzer);
			final String line = disease + "";
			final Query query = parser.parse(line);

			final TopScoreDocCollector collector = TopScoreDocCollector.create(
					hitsPerPage, true);

			searcher.search(query, collector);
			final ScoreDoc[] results = collector.topDocs().scoreDocs;
			final ArrayList<Document> docs = new ArrayList<>();
			String s = "";
			for (final ScoreDoc result : results) {
				final int DocId = result.doc;
				docs.add(searcher.doc(DocId));
				for (final Document d : docs) {
					if (d.get("Symptom") != null) {

						resultD.getSymptoms().add(d.get("Symptom"));
						diseaseText = true;
						resultD.addSource("OMIM");
					}
				}

			}
			if (docs.get(0).get("disease") != null) {
				final String values[] = docs.get(0).get("disease").split(";");
				final String values1[] = values[0].split(",");
				final String values2[] = values1[0].split(" ");
				for (int i = 1; i < values2.length; i++) {
					s = s.concat(values2[i] + " ");

				}
				resultD.setDisease(s);
			}

			reader.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// :Post-Release-Update-Version.LUCENE_XY:
		catch (final ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}