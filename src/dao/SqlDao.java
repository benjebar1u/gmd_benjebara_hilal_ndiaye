package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import objects.DiseaseResult;
import objects.DrugResult;

public class SqlDao {

	static String DB_SERVER = "jdbc:mysql://neptune.telecomnancy.univ-lorraine.fr:3306/";
	static String DB = "gmd";
	static String DRIVER = "com.mysql.jdbc.Driver";
	static String USER_NAME = "gmd-read";
	static String USER_PSWD = "esial";

	Connection jdbcConnection = null;
	
	public static boolean diseaseSQL = false;
	public static boolean drugSQL = false;

	public SqlDao() {
		try {
			Class.forName(DRIVER);
			jdbcConnection = DriverManager.getConnection(DB_SERVER + DB,
					USER_NAME, USER_PSWD);
		} catch (final ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Retrieve the causing drugs if the given disease
	 * 
	 * @param diseaseToFind
	 * @param result
	 */
	public void findCausingDrugsByDisease(String diseaseToFind,
			final DiseaseResult result) {

		diseaseToFind = diseaseToFind.replace('*', '%');

		final String FIND_CAUSING_DRUGS_BY_DISEASE = "SELECT distinct a.drug_name2 ,b.se_name FROM label_mapping a,adverse_effects_raw b WHERE b.label=a.label AND b.se_name LIKE ?";

		try {
			final PreparedStatement causingDrugsStatement = jdbcConnection
					.prepareStatement(FIND_CAUSING_DRUGS_BY_DISEASE);
			causingDrugsStatement.setString(1, diseaseToFind);
			final ResultSet causingDrugs = causingDrugsStatement.executeQuery();
			final ArrayList<String> causingDrugsList = new ArrayList<String>();
			while (causingDrugs.next()) {
				causingDrugsList.add(causingDrugs.getString("DRUG_NAME2"));
				result.setDisease(causingDrugs.getString("SE_NAME"));
				//diseaseSQL = true;
				result.addSource("Sider 2");
			}

			result.setCausingDrugs(causingDrugsList);

		} catch (final SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void findCuredDiseaseByDrug(String drugToFind,
			final DrugResult result) {
		drugToFind = drugToFind.replace('*', '%');
		final String FIND_CURED_DISEASES_BY_DRUG = "SELECT distinct b.i_name,a.drug_name2 FROM label_mapping a,indications_raw b WHERE b.label=a.label AND a.drug_name2 LIKE ?";

		try {
			final PreparedStatement curedDiseaseStatement = jdbcConnection
					.prepareStatement(FIND_CURED_DISEASES_BY_DRUG);
			curedDiseaseStatement.setString(1, drugToFind);
			final ResultSet curedDisease = curedDiseaseStatement.executeQuery();
			final ArrayList<String> curedDiseasesList = new ArrayList<String>();
			while (curedDisease.next()) {
				curedDiseasesList.add(curedDisease.getString("I_NAME"));
				result.setDrug(curedDisease.getString("DRUG_NAME2"));
				//drugSQL = true;
				result.addSource("Sider 2");
			}

			result.setCuredDiseases(curedDiseasesList);

		} catch (final SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Retrieve the curing drugs if the given disease
	 * 
	 * @param diseaseToFind
	 * @param result
	 */
	public void findCuringDrugsByDisease(String diseaseToFind,
			final DiseaseResult result) {

		diseaseToFind = diseaseToFind.replace('*', '%');
		final String FIND_CURING_DRUGS_BY_DISEASE = "SELECT a.drug_name2,b.i_name FROM label_mapping a,indications_raw b WHERE b.label=a.label AND b.i_name LIKE ?";
		try {
			final PreparedStatement curingDrugsStatement = jdbcConnection
					.prepareStatement(FIND_CURING_DRUGS_BY_DISEASE);
			curingDrugsStatement.setString(1, diseaseToFind);
			final ResultSet curingDrugs = curingDrugsStatement.executeQuery();
			final ArrayList<String> curingDrugsList = new ArrayList<String>();
			while (curingDrugs.next()) {
				curingDrugsList.add(curingDrugs.getString("DRUG_NAME2"));
				result.setDisease(curingDrugs.getString("I_NAME"));
				//diseaseSQL = true;
				result.addSource("Sider 2");
			}
			result.setCuringDrugs(curingDrugsList);

		} catch (final SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void findSideEffectsByDrug(String drugToFind, final DrugResult result) {
		drugToFind = drugToFind.replace('*', '%');
		final String FIND_SIDE_EFFECTS_BY_DRUG = "SELECT distinct b.se_name,a.drug_name2 FROM label_mapping a,adverse_effects_raw b WHERE b.label=a.label AND a.drug_name2 LIKE ?";

		try {
			final PreparedStatement sideEffectStatement = jdbcConnection
					.prepareStatement(FIND_SIDE_EFFECTS_BY_DRUG);
			sideEffectStatement.setString(1, drugToFind);
			final ResultSet sideEffects = sideEffectStatement.executeQuery();
			final ArrayList<String> sideEffectsList = new ArrayList<String>();
			while (sideEffects.next()) {
				sideEffectsList.add(sideEffects.getString("SE_NAME"));
				result.setDrug(sideEffects.getString("DRUG_NAME2"));
				drugSQL = true;
			}

			result.setSideEffects(sideEffectsList);

		} catch (final SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Connection getJdbcConnection() {
		return jdbcConnection;
	}

}