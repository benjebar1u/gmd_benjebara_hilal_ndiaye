package objects;

import dao.SaxReader;
import dao.SqlDao;

import java.util.ArrayList;

import org.json.JSONArray;

public class DrugResult {
	String drug;

	ArrayList<String> curedDiseases;

	ArrayList<String> sideEffects;
	
	ArrayList<String> source = new ArrayList<String>();

	public DrugResult(String drug, ArrayList<String> curedDiseases, ArrayList<String> sideEffects) {
		super();
		this.drug = drug;
		this.curedDiseases = curedDiseases;
		this.sideEffects = sideEffects;
	}

	public DrugResult(String drugToFind) {
		super();
		this.drug = drugToFind;
		curedDiseases = new ArrayList<String>();
		sideEffects = new ArrayList<String>();
	}

	public String getDrug() {
		return drug;
	}

	public void setDrug(String drug) {
		this.drug = drug;
	}

	public ArrayList<String> getCuredDiseases() {
		return curedDiseases;
	}

	public void setCuredDiseases(ArrayList<String> curedDiseases) {
		this.curedDiseases = curedDiseases;
	}

	public ArrayList<String> getSideEffects() {
		return sideEffects;
	}

	public void setSideEffects(ArrayList<String> sideEffects) {
		this.sideEffects = sideEffects;
	}

	public void addSource(String s){
		this.source.add(source.size(), s);
	}
	
	public void setSource(ArrayList<String> source) {
		this.source = source;
	}
	
	public ArrayList<String> getSource() {
		ArrayList<String> list = new ArrayList<String>();
		for(int i=0; i<source.size(); i++){
			String src_i = source.get(i);
			list.add(src_i);
	       	int j= i+1;
	       	// Delete the duplicates
	       	while(j<source.size()){
	       		String src_j = source.get(j);
	       		if(!src_j.equals(src_i)){
	       			list.add(src_j);
	       			
	       		}else{
	       			j++;
	       		}
	       			
	     	}
	    }
	
		for(int i=0;i<list.size();i++){
			System.out.println(list.get(i));
		}	
		
		return source;
	}

	
	@Override
	public String toString() {
		System.out.println("Drug : " + drug);
		System.out.println("Cured Diseases : " + new JSONArray(curedDiseases).toString());
		System.out.println("side Effects : " + new JSONArray(sideEffects).toString());
		System.out.println("Source: " + getSource());
		
		return drug;
	}

}