package objects;

import java.util.ArrayList;

import services.DiseaseManagerService;

public class Main {

	static ArrayList<DiseaseResult> results = new ArrayList<>();

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		/*
		 * String input = "AARSKOG"; List<String> inputStream =
		 * Arrays.asList(input.split(" ")); for (int i = 0; i <
		 * inputStream.size(); i++) { String str = inputStream.get(i); if
		 * (str.equals("et")) { find(inputStream.get(i + 1));
		 * find(inputStream.get(i - 1)); for (DiseaseResult result : results) {
		 * print(result); } } } }
		 * 
		 * private static void print(DiseaseResult result) { result.toString();
		 * }
		 * 
		 * private static DiseaseResult find(String disease) { DiseaseRequest
		 * request = new DiseaseRequest(disease); DiseaseManagerService service
		 * = new DiseaseManagerService(request);
		 * 
		 * DiseaseResult foundResult = service
		 * .findInformationForGivenDisease(request); if
		 * (!results.contains(foundResult) && !foundResult.isEmpty()) {
		 * results.add(foundResult); } return foundResult;
		 */

		final String disease = "Angelman syndrome";
		final DiseaseRequest request = new DiseaseRequest(disease);
		final DiseaseManagerService service = new DiseaseManagerService(request);
		service.findInformationForGivenDisease(request).toString();

		// final String drug = "Cet*ximab";
		// final DrugManagerService service = new DrugManagerService(drug);
		// service.findInformationForGivingDrug(drug).toString();
		//

	}

}
