package objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Compute the individual disease results in order to fit the composed request
 * matching the AND et the OR
 * 
 * 
 */
public class ResultComputing {

	/**
	 * The logical or operator
	 */
	private static final String OR = "ou";

	/**
	 * The logical and operator
	 */
	private static final String AND = "et";

	/**
	 * Compute the individual disease results in order to fit the composed
	 * request matching the AND et the OR
	 * 
	 * @param input
	 * @param results
	 * @return a set of disease result
	 */
	public static Set<DiseaseResult> computeDiseaseResult(final String input,
			final Map<String, DiseaseResult> results) {

		// List for the OR composition
		final Set<DiseaseResult> orComputedResults = new HashSet<>(); // Hashet
																		// is to
																		// ensure
																		// unicity
		// List for the and composition
		final LinkedHashSet<DiseaseResult> andComputedResults = new LinkedHashSet<>(); // linkedhashet
																						// to
																						// ensure
																						// unicity
																						// and
																						// low
																						// cost

		// We put all the strings in the input in an array
		final List<String> inputStream = Arrays.asList(input.split(" "));
		final List<String> formatedInputStream = new ArrayList<>();
		formatedInputStream.addAll(inputStream);
		formatInput(inputStream, formatedInputStream);

		if (formatedInputStream.size() == 1) {
			orComputedResults.add(results.get(formatedInputStream.get(0)));
			return orComputedResults;
		}
		for (int i = 0; i < formatedInputStream.size(); i++) {
			final String word = formatedInputStream.get(i);
			if (AND.equals(word)) {
				// If none of the adjacent strings is empty, we add them both to
				// the result
				if (!results.get(formatedInputStream.get(i - 1)).isEmpty()
						&& !results.get(formatedInputStream.get(i + 1))
								.isEmpty()) {
					andComputedResults.add(results.get(formatedInputStream
							.get(i - 1)));
					andComputedResults.add(results.get(formatedInputStream
							.get(i + 1)));
				}
				// If one of them is empty, the "and chain" is broken so we
				// clear the list
				if (results.get(formatedInputStream.get(i + 1)).isEmpty()
						|| results.get(formatedInputStream.get(i - 1))
								.isEmpty()) {
					andComputedResults.clear();
				}
			} else if (OR.equals(word)) {
				// Or is fine, we just need to make sure that an empty result is
				// not added
				if (!results.get(formatedInputStream.get(i - 1)).isEmpty()) {
					orComputedResults.add(results.get(formatedInputStream
							.get(i - 1)));
				}
				if (results.get(formatedInputStream.get(i + 1)) != null) {
					if (!results.get(formatedInputStream.get(i + 1)).isEmpty()) {
						orComputedResults.add(results.get(formatedInputStream
								.get(i + 1)));
					}
				}
			}
		}
		// We merge the two lists
		orComputedResults.addAll(andComputedResults);
		return orComputedResults;

	}

	private static void formatInput(final List<String> inputStream,
			final List<String> formatedInputStream) {
		for (int i = 1; i < inputStream.size(); i++) {
			if (isNotAnOperator(inputStream.get(i))
					&& isNotAnOperator(inputStream.get(i - 1))) {
				formatedInputStream.remove(i - 1);
				formatedInputStream.set(i - 1, inputStream.get(i - 1) + " "
						+ inputStream.get(i));
			}
		}
	}

	public static boolean isNotAnOperator(final String str) {
		return !AND.equals(str) && !OR.equals(str);
	}

}
