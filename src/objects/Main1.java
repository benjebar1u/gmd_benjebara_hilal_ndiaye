package objects;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import services.DiseaseManagerService;

public class Main1 {

	private static DiseaseResult find(final String disease) {
		final DiseaseRequest request = new DiseaseRequest(disease);
		final DiseaseManagerService service = new DiseaseManagerService(request);
		return service.findInformationForGivenDisease(request);
	}

	public static void main(final String[] args) {
		final String input = "AARSKOG";
		final Map<String, DiseaseResult> results = new HashMap<>();
		final List<String> diseasesToSearch = Arrays.asList(input
				.split(" et | ou "));

		for (final String disease : diseasesToSearch) {
			results.put(disease, find(disease));
		}

		for (final DiseaseResult result : ResultComputer.computeDiseaseResult(
				input, results)) {
			result.toString();
		}
	}

}
