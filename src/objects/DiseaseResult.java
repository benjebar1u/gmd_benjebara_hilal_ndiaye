package objects;

import dao.CouchDao;
import dao.SaxReader;
import dao.SqlDao;
import dao.TxtReader;

import java.util.ArrayList;

public class DiseaseResult {

	String disease;

	ArrayList<String> curingDrugs;

	ArrayList<String> causingDrugs;

	ArrayList<String> symptoms;
	ArrayList<String> source = new ArrayList<String>();

	public DiseaseResult(final String diseaseTofind) {
		disease = diseaseTofind;
		causingDrugs = new ArrayList<String>();
		curingDrugs = new ArrayList<String>();
		symptoms = new ArrayList<String>();
	}

	public DiseaseResult(final String disease,
			final ArrayList<String> curingDrugs,
			final ArrayList<String> causingDrugs,
			final ArrayList<String> symptoms) {
		super();
		this.disease = disease;
		this.curingDrugs = curingDrugs;
		this.causingDrugs = causingDrugs;
		this.symptoms = symptoms;
	}

	public ArrayList<String> getCausingDrugs() {
		return causingDrugs;
	}

	public ArrayList<String> getCuringDrugs() {
		return curingDrugs;
	}

	public String getDisease() {
		return disease;
	}

	public ArrayList<String> getSymptoms() {
		return symptoms;
	}

	public boolean isEmpty() {
		return symptoms.isEmpty() && causingDrugs.isEmpty()
				&& curingDrugs.isEmpty();

	}

	public void setCausingDrugs(final ArrayList<String> causingDrugs) {
		this.causingDrugs = causingDrugs;
	}

	public void setCuringDrugs(final ArrayList<String> curingDrugs) {
		this.curingDrugs = curingDrugs;
	}

	public void setDisease(final String disease) {
		this.disease = disease;
	}

	public void setSymptoms(final ArrayList<String> symptoms) {
		this.symptoms = symptoms;
	}
	
	public void setSource(ArrayList<String> source) {
		this.source = source;
	}
	
	public void addSource(String s){
		this.source.add(source.size(), s);
	}
	
	
	
	public ArrayList<String> getSource() {
		ArrayList<String> list = new ArrayList<String>();
		for(int i=0; i<source.size(); i++){
			String src_i = source.get(i);
			list.add(src_i);
	       	int j= i+1;
	       	// Delete the duplicates
	       	while(j<source.size()){
	       		String src_j = source.get(j);
	       		if(!src_j.equals(src_i)){
	       			list.add(src_j);
	       			
	       		}else{
	       			j++;
	       		}
	       			
	     	}
	    }
	
		for(int i=0;i<list.size();i++){
			System.out.println(list.get(i));
		}	
		
		return source;
	}


	@Override
	public String toString() {

		// System.out.println("Disease : " + disease);
		// System.out.println("Curing drugs : "
		// + new JSONArray(curingDrugs).toString());
		// System.out.println("Causing drugs : "
		// + new JSONArray(causingDrugs).toString());
		// System.out.println("Symptoms : " + new
		// JSONArray(symptoms).toString());
		// // System.out.println("Source: " + new JSONArray(source).toString());

		System.out.println("Disease :" + disease);
		System.out
				.println("-------------------------------------------------------------------------------Curing drugs :");
		for (final String s : curingDrugs) {
			System.out.println(s);
		}
		System.out
				.println("----------------------------------------------------------------------------Causing drugs :");
		for (final String s : causingDrugs) {
			System.out.println(s);
		}
		System.out
				.println("---------------------------------------------------------------------------------Symptoms :");
		for (final String s : symptoms) {
			System.out.println(s);
		}
		System.out
			.println("---------------------------------------------------------------------------------Sources :");
		for (final String s : source) {
			System.out.println(s);
		}
		
	
		return disease;

	}

}