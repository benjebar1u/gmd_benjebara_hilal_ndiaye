package objects;

import java.util.ArrayList;

public class DiseaseRequest {
	/**
	 * The disease to find
	 */
	String disease;
	ArrayList <String>diseases;
	
	public DiseaseRequest(String disease) {
		super();
		this.disease = disease;
	}

	public String getDisease() {
		// TODO Auto-generated method stub
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public ArrayList<String> getDiseases() {
		return diseases;
	}

	public void setDiseases(ArrayList<String> diseases) {
		this.diseases = diseases;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
	

}
