package utils;

public class TxtObject {
	private String Number, diseaseName, symptoms;

	public TxtObject() {

	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getNumber() {
		return Number;
	}

	public void setNumber(String number) {
		Number = number;
	}

	public String getDiseaseName() {
		return diseaseName;
	}

	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}

	public void print() {
		System.out.println(Number);
		System.out.println(diseaseName);

	}
}
