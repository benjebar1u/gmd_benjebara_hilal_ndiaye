package utils;

import objects.DiseaseRequest;
import services.DiseaseManagerService;


public class ResquestBuilder {
	String input;
	String [] valuesWithAnd=input.split(" et ");
	
	public ResquestBuilder(String input){
		this.input=input;
	}

	public void requestWithAnd(String [] valuesWithAnd){
		for(String s: valuesWithAnd){
			DiseaseRequest request = new DiseaseRequest(s);
			DiseaseManagerService service=new DiseaseManagerService(request);
			service.findInformationForGivenDisease(request);	
		}
	}
}
