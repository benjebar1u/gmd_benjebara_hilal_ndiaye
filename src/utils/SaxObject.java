package utils;

public class SaxObject {
	private String drugbankId, name, indication, toxicity, synonym, type;

	public SaxObject() {
	}

	public String getDrugbankId() {
		return drugbankId;
	}

	public void setDrugbankId(String drugbankId) {
		this.drugbankId = drugbankId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getToxicity() {
		return toxicity;
	}

	public void setToxicity(String toxicity) {
		this.toxicity = toxicity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSynonym() {
		return synonym;
	}

	public void setSynonym(String synonym) {
		this.synonym = synonym;
	}

	@Override
	public String toString() {
		return new StringBuffer("drugbankId : ").append(drugbankId).append(", ").append("name : ").append(name)
				.append(", ").append("indication : ").append(indication).append(", ").append("toxicity : ")
				.append(toxicity).append(", ").append("type : ").append(type).append(", ").append("synonym : ")
				.append(synonym).toString();
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

}
