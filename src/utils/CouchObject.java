package utils;

import java.util.ArrayList;

public class CouchObject {
	String Name;
	ArrayList<String> Synonyms;
	int OrphaNumber;
	ArrayList<String> Symptoms;

	public CouchObject() {

	}

	public CouchObject(String name, ArrayList<String> synonym, int orphaNumber, ArrayList<String> symptoms) {
		super();
		Name = name;
		Synonyms = synonym;
		OrphaNumber = orphaNumber;
		Symptoms = symptoms;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public ArrayList<String> getSynonym() {
		return Synonyms;
	}

	public void setSynonym(ArrayList<String> synonym) {
		Synonyms = synonym;
	}

	public int getOrphaNumber() {
		return OrphaNumber;
	}

	public void setOrphaNumber(int orphaNumber) {
		OrphaNumber = orphaNumber;
	}

	public ArrayList<String> getSymptoms() {
		return Symptoms;
	}

	public void setSymptoms(ArrayList<String> symptoms) {
		Symptoms = symptoms;
	}

}
